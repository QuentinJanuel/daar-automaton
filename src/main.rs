#[macro_use] extern crate lalrpop_util;

mod fsm;

lalrpop_mod!(pub grammar);

#[derive(Debug)]
pub enum Regex {
    Atom(String),
    Star(Box<Regex>),
    Concat(Box<Regex>, Box<Regex>),
    Or(Box<Regex>, Box<Regex>),
}

fn merge_unique(a_chars: Vec<String>, b_chars: Vec<String>) -> Vec<String> {
    let mut a_chars = a_chars;
    for b_char in &b_chars {
        if !a_chars.contains(b_char) {
            a_chars.push(b_char.clone());
        }
    }
    a_chars
}

impl Regex {
    pub fn chars(&self) -> Vec<String> {
        match self {
            Self::Atom(a) => vec![a.clone()],
            Self::Star(a) => a.chars(),
            Self::Concat(a, b) => {
                merge_unique(
                    a.chars(),
                    b.chars(),
                )
            },
            Self::Or(a, b) => {
                merge_unique(
                    a.chars(),
                    b.chars(),
                )
            },
        }
    }
}

fn regex2FSM(regex: &Regex) -> fsm::FSM {
    match regex {
        Regex::Atom(c) => {
            let char = fsm::Char::Letter(c.clone());
            let chars = vec![char];
            let mut fstate = fsm::State::new();
            fstate.set_final(true);
            let mut istate = fsm::State::new();
            let trans = fsm::Trans::new(0, 1);
            istate.push_trans(trans);
            let states = vec![istate, fstate];
            let fsm = fsm::FSM::new(0, states, chars);
            fsm
        },
        Regex::Star(a) => todo!(),
        Regex::Concat(a, b) => {
            let fsmA = regex2FSM(a);
            let fsmB = regex2FSM(b);
            let eps = fsm::Char::Epsilon;
            let trans = fsm::Trans::new(0, 1);
            todo!()
        },
        Regex::Or(a, b) => todo!(),
    }
}

fn main() {
    let parser = grammar::RegexParser::new();
    match parser.parse("ab|c*cdb") {
        Ok(r) => {
            println!("Parsed: {:?}", r);
            let chars = r.chars();
            println!("Chars: {:?}", chars);
            let automaton = regex2FSM(&r);
            println!("Automaton: {:?}", automaton);
        },
        Err(err) => panic!("Failed to parse, {}", err),
    };
}
