#[derive(Debug, Clone)]
pub enum Char {
    Letter(String),
    Epsilon,
}

#[derive(Debug)]
pub struct Trans {
    char: usize,
    dest: usize,
}

impl Trans {
    pub fn new(char: usize, dest: usize) -> Self {
        Self { char, dest }
    }
}

#[derive(Debug)]
pub struct State {
    is_final: bool,
    trans: Vec<Trans>,
}

impl State {
    pub fn new() -> Self {
        Self {
            is_final: false,
            trans: vec![],
        }
    }
    pub fn set_final(&mut self, f: bool) {
        self.is_final = f;
    }
    pub fn push_trans(&mut self, t: Trans) {
        self.trans.push(t);
    }
}

#[derive(Debug)]
pub struct FSM {
    init: usize,
    states: Vec<State>,
    chars: Vec<Char>,
}

impl FSM {
    pub fn new(init: usize, states: Vec<State>, chars: Vec<Char>) -> Self {
        Self { init, states, chars, }
    }
}
